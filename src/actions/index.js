import axios from 'axios'

const url = 'https://api.nasa.gov/neo/rest/v1';
const api_key = 'o0428KYs10pjp3FUnozm63THGzwcHPThAJoXHZsA';

const startFetch = () => { return { type: "IS_FETCHING", isFetching: true }};
const errorFetch = (err) => { return { type: 'ERROR_FETCH', isFetching: false, err }};
const completeFetch = (data) => { return { type: 'COMPLETE_FETCH', isFetching: false, payload: data }};
const completeFetchSearch = (data) => { return { type: 'COMPLETE_FETCH_SEARCH', isFetching: false, payload: data }};
const completeFetchAsteroid = (data) => { return { type: 'COMPLETE_FETCH_ASTEROID', isFetching: false, payload: data  }};

export const listAsteroids = () => {
  return (dispatch, getState) => {
    dispatch(startFetch());
    axios.get(`${url}/feed?start_date&end_date&api_key=${api_key}`)
    .then(response => {
      console.log(response.data);
      dispatch(completeFetch(response.data));
    })
    .catch(error => {
      dispatch(errorFetch(error));
    });
  }
}

export const listAsteroidUnity = (id) => {
  return (dispatch, getState) => {
    dispatch(startFetch());
    axios.get(`${url}/neo/${id}?api_key=${api_key}`)
    .then(response => {
      dispatch(completeFetchAsteroid(response.data));
    })
    .catch(error => {
      dispatch(errorFetch(error));
    });
  }
}

export const searchAsteroids = (date) => {
  const { since, until } = date;
  return (dispatch, getState) => {
    dispatch(startFetch());
    axios.get(`${url}/feed?start_date=${since}&end_date=${until}&api_key=${api_key}`)
    .then(response => {
      dispatch(completeFetchSearch(response.data));
    })
    .catch(error => {
      dispatch(errorFetch(error));
    });
  }
}
