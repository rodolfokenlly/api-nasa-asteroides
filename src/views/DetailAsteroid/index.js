import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import { listAsteroidUnity } from '../../actions'
import './index.css'

const mapStateToProps = (state) => {
  return {
    asteroid: state.asteroids
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    listAsteroidUnity
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(
  class DetailAsteroid extends Component {
    constructor(props) {
      super(props);
      this.state = {
        idAsteroid: this.props.match.params.asteroidId
      };
    }

    componentWillMount() {
      this.props.listAsteroidUnity(this.state.idAsteroid);
    }

    render() {
      if (this.props.asteroid.type === 'COMPLETE_FETCH_ASTEROID') {
        console.log(this.props);
        console.log(this.props.asteroid);
      }
      return(
        <div style={{color: "black"}}>
          COMPONENT DETAILASTEROID Es: {this.state.idAsteroid}
        </div>
      )
    }
  }
)