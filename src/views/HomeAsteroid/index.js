import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components'
import './index.css'

const Button = styled.button`
  height: 50px;
  width: 200px;
  padding-top: 10px;
  padding-bottom: 10px;
  padding-left: 20px;
  padding-right: 20px;
  text-align: center;
  background-color: #33ff5e;
  color: #fff;
  font-family: 'Roboto', sans-serif;
  font-size: 20px;
  border: none;
  border-radius: 8px;
  cursor: pointer;
`;

export default class HomeAsteroid extends Component {
  constructor() {
    super();
    this.state = { nextView: false };
    this.handleClickEntry = this.handleClickEntry.bind(this);
  }

  handleClickEntry(e) {
    this.setState({ nextView: true });
  }

  render() {
    if (this.state.nextView) {
      return (
        <Redirect to='/listAsteroids' />
      );
    }
    return(
      <div className="HomeAsteroid">
        <div className="HomeAsteroid-content">
          <div className="HomeAsteroid-title">
            <h1>Asteroides Cercanos a la tierra</h1>
          </div>
          <div className="HomeAsteroid-information">
            <p className="HomeAsteroid-information-paraph">
              NeoWs (Servicio web de objetos cercanos a la Tierra) es un servicio web RESTful 
              para información de asteroides cercanos a la tierra. Con NeoWs, un usuario puede:
              buscar asteroides en función de su fecha de acercamiento más cercana a la Tierra, 
              buscar un asteroide específico con su identificación de cuerpo pequeño de NASA JPL, 
              así como navegar por el conjunto de datos general.
            </p>
            <Button onClick={this.handleClickEntry}>ENTRAR</Button>
          </div>
        </div>
      </div>
    )
  }
}