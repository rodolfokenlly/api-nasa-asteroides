import React, { Component } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-dom'
import styled from 'styled-components'

import { listAsteroids } from '../../actions'
import ItemAsteroid from '../../components/ItemAsteroid'

import 'materialize-css/dist/css/materialize.min.css'
import './index.css'

const Title = styled.h1`
  color: rebeccapurple;
  font-family: 'Trebuchet MS';
  text-align: center;
  margin: 20px;
`;

class ListAsteroid extends Component {
  constructor() {
    super();
    this.state = {
      nextViewDetail: false,
      idAsteroid: 0
    };
    this.handleClickDetail = this.handleClickDetail.bind(this);
  }

  componentWillMount() {
    this.props.listAsteroids();
  }

  handleClickDetail(Id) {
    this.setState({ nextViewDetail: true, idAsteroid: Id });
  }

  loadDataAsteroids() {
    return (
      this.props.asteroids.payload.map((a) => {
        return <ItemAsteroid
          key={a.Id.toString()}
          Id={a.Id}
          Name_Asteroid={a.Name_Asteroid}
          Url_Jpl_Nasa={a.Url_Jpl_Nasa}
          Magnitude_Absolute={a.Magnitude_Absolute}
          Diameters_Estimated={a.Diameters_Estimated}
          Potentially_Dangerous={a.Potentially_Dangerous}
          Approach_Data={a.Approach_Data}
          handleClickDetail={this.handleClickDetail}
        />
      })
    )
  }

  render() {
    let asteroids = [];
    if (this.state.nextViewDetail) {
      return (
        <Redirect to={`/detailAsteroid/${this.state.idAsteroid}`}/>
      )
    }
    if (this.props.asteroids.type === 'COMPLETE_FETCH') {
      asteroids = this.loadDataAsteroids();
    }
    return(
      <div>
        <Title>
          Asteroides cercanos a la Tierra <span aria-label="🌍" role="img" />
        </Title>
        <div className="container">
          <div className="row">
            {asteroids}
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    asteroids: state.asteroids
  };
}

const mapDispatchToProps = (dispatch) => {
  return bindActionCreators({
    listAsteroids
  }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(ListAsteroid);