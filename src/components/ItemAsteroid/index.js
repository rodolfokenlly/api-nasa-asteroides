import React, { Component } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

import 'materialize-css/dist/css/materialize.min.css'
import './index.css'

const Button = styled.button`
  height: 2em;
  width: 20vh;
  padding-top: 4px;
  padding-bottom: 4px;
  padding-left: 4px;
  padding-right: 4px;
  text-align: center;
  background-color: #33ff5e;
  color: #fff;
  font-family: 'Roboto', sans-serif;
  font-size: 16px;
  border: none;
  border-radius: 8px;
  cursor: pointer;
`;

export default class ItemAsteroid extends Component {
  constructor() {
    super();
    this.state = {};
    this.handleClickDetailAsteroid = this.handleClickDetailAsteroid.bind(this);
  }

  handleClickDetailAsteroid() {
    this.props.handleClickDetail(this.props.Id);
  }

  render() {
    return(
      <div className="col xl3 l3 m6 s12">
        <div className="card ItemAsteroid">
          <div className="card-content">
            <h6>{this.props.Name_Asteroid}</h6>
            <h6>{this.props.Url_Jpl_Nasa}</h6>
            <Button onClick={this.handleClickDetailAsteroid}>
              Ver Detalle
            </Button>
          </div>
        </div>
      </div>
    )
  }
}

ItemAsteroid.propTypes = {
  Id: PropTypes.number.isRequired,
  Name_Asteroid: PropTypes.string.isRequired,
  Url_Jpl_Nasa: PropTypes.string.isRequired,
  Magnitude_Absolute: PropTypes.number.isRequired,
  Diameters_Estimated: PropTypes.object.isRequired,
  Potentially_Dangerous: PropTypes.bool.isRequired,
  Approach_Data: PropTypes.object.isRequired,
}