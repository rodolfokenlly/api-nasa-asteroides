import React, { Component } from 'react'
import { Router, Switch , Route } from 'react-router-dom'
import PropTypes from 'prop-types'

import HomeAsteroid from './views/HomeAsteroid'
import ListAsteroids from './views/ListAsteroids'
import DetailAsteroid from './views/DetailAsteroid'

export default class App extends Component {
  render() {
    return (
      <Router history={this.props.history}>
        <Switch>
          <Route exact path="/" component={HomeAsteroid} />
          <Route path="/listAsteroids" component={ListAsteroids} />
          <Route path="/detailAsteroid/:asteroidId" component={DetailAsteroid} />
        </Switch>
      </Router>
    )
  }
}

App.propTypes = {
  history: PropTypes.any
}
