import { mapAsteroidsConvert} from '../helpers'

export default function asteroids(state = [], action) {
  switch (action.type) {
    case 'COMPLETE_FETCH':
      action.payload = mapAsteroidsConvert(action);
      return action;
    case 'COMPLETE_FETCH_SEARCH':
      return action;
    case 'COMPLETE_FETCH_ASTEROID':
      action.payload = mapAsteroidsConvert(action);
      return action;
    case 'IS_FETCHING':
      return action;
    case 'ERROR_FETCH':
      return action;
    default:
      return action;
  }
}