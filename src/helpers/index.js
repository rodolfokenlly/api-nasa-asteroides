export const mapAsteroidsConvert = ({ payload }) => {
  let newPayload = [];

  if (typeof payload.near_earth_objects !== 'undefined') {
    let keys = Object.keys(payload.near_earth_objects);
    keys.map((k) => {
      payload.near_earth_objects[k].map((neartEarth) => {
        newPayload = [...newPayload, {
          Id: parseInt(neartEarth.id),
          Name_Asteroid: neartEarth.name,
          Url_Jpl_Nasa: neartEarth.nasa_jpl_url,
          Magnitude_Absolute: neartEarth.absolute_magnitude_h,
          Diameters_Estimated: {
            Kilometers: {
              Minimum: neartEarth.estimated_diameter.kilometers.estimated_diameter_min,
              Maximum: neartEarth.estimated_diameter.kilometers.estimated_diameter_max
            },
            Meters: {
              Minimum: neartEarth.estimated_diameter.meters.estimated_diameter_min,
              Maximum: neartEarth.estimated_diameter.meters.estimated_diameter_max
            },
            Miles: {
              Minimum: neartEarth.estimated_diameter.miles.estimated_diameter_min,
              Maximum: neartEarth.estimated_diameter.miles.estimated_diameter_max
            },
            Feet: {
              Minimum: neartEarth.estimated_diameter.feet.estimated_diameter_min,
              Maximum: neartEarth.estimated_diameter.feet.estimated_diameter_max
            }
          },
          Potentially_Dangerous: neartEarth.is_potentially_hazardous_asteroid,
          Approach_Data: {
            Date_Approach: neartEarth.close_approach_data[0].close_approach_date,
            Epoch: neartEarth.close_approach_data[0].epoch_date_close_approach,
            Velocity_Relative: {
              Kilometers_Second: neartEarth.close_approach_data[0].relative_velocity.kilometers_per_second,
              Kilometers_Hour: neartEarth.close_approach_data[0].relative_velocity.kilometers_per_hour,
              Miles_Hour: neartEarth.close_approach_data[0].relative_velocity.miles_per_second
            },
            Distance_Miss: {
              Astronomy: neartEarth.close_approach_data[0].miss_distance.astronomical,
              Lunar: neartEarth.close_approach_data[0].miss_distance.lunar,
              Kilometers: neartEarth.close_approach_data[0].miss_distance.kilometers,
              Miles: neartEarth.close_approach_data[0].miss_distance.miles
            },
            Body_Orbiting: neartEarth.close_approach_data[0].orbiting_body
          }
        }];
      })
    })
  } else {
    newPayload = [...newPayload, {
      Id: parseInt(payload.id),
      Name_Asteroid: payload.name,
      Url_Jpl_Nasa: payload.nasa_jpl_url,
      Magnitude_Absolute: payload.absolute_magnitude_h,
      Diameters_Estimated: {
        Kilometers: {
          Minimum: payload.estimated_diameter.kilometers.estimated_diameter_min,
          Maximum: payload.estimated_diameter.kilometers.estimated_diameter_max
        },
        Meters: {
          Minimum: payload.estimated_diameter.meters.estimated_diameter_min,
          Maximum: payload.estimated_diameter.meters.estimated_diameter_max
        },
        Miles: {
          Minimum: payload.estimated_diameter.miles.estimated_diameter_min,
          Maximum: payload.estimated_diameter.miles.estimated_diameter_max
        },
        Feet: {
          Minimum: payload.estimated_diameter.feet.estimated_diameter_min,
          Maximum: payload.estimated_diameter.feet.estimated_diameter_max
        }
      },
      Potentially_Dangerous: payload.is_potentially_hazardous_asteroid,
      Approach_Data: {
        Date_Approach: payload.close_approach_data[0].close_approach_date,
        Epoch: payload.close_approach_data[0].epoch_date_close_approach,
        Velocity_Relative: {
          Kilometers_Second: payload.close_approach_data[0].relative_velocity.kilometers_per_second,
          Kilometers_Hour: payload.close_approach_data[0].relative_velocity.kilometers_per_hour,
          Miles_Hour: payload.close_approach_data[0].relative_velocity.miles_per_second
        },
        Distance_Miss: {
          Astronomy: payload.close_approach_data[0].miss_distance.astronomical,
          Lunar: payload.close_approach_data[0].miss_distance.lunar,
          Kilometers: payload.close_approach_data[0].miss_distance.kilometers,
          Miles: payload.close_approach_data[0].miss_distance.miles
        },
        Body_Orbiting: payload.close_approach_data[0].orbiting_body
      }
    }];
  }

  return newPayload;
}